const passport = require('passport');
var passwordless = require('passwordless');

exports.index= (req,res,next)=>{
    res.render('index');    
};
exports.signupget = (req,res,next)=>{
    res.render('registrarse');
};
exports.signup= passport.authenticate('local-signup',{
    successRedirect:'/home',
    failureRedirect: '/signup',
    passReqToCallback: true
});
exports.signinget= (req,res,next)=>{
    res.render('login');
};
exports.signinpost= passport.authenticate('local-signin',{
    successRedirect:'/home',
    failureRedirect: '/signin',
    passReqToCallback: true
});
exports.home = (req, res,next)=>{
    res.render('home');
};
exports.logout = (req,res,next)=>{
    req.logout();
    passwordless.logout();
    res.redirect('/');
};
exports.authgoogle = passport.authenticate('google', 
    {scope: ['profile', 'email']});

exports.googlecallback = passport.authenticate('google',
    { successRedirect: '/home',
    failureRedirect: '/' });

exports.loginPasswordless = (req,res,next) =>{
    res.render('loginps');
};