const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const Schema = mongoose.Schema;
const userSchema = new Schema({
    local: {
        name: String,
        email: String,
		password: String
	},
	google: {
		id: String,
		token: String,
		email: String,
		name: String
}
});
userSchema.methods.encryptPassword = function(password){
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
};
userSchema.methods.validatePassword =function (password){
    return bcrypt.compareSync(password,this.local.password);
};

module.exports = mongoose.model('user',userSchema);