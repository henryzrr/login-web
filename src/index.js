const express = require('express');
const app = express();

const path = require('path');
const morgan = require('morgan');
const mongoose = require('mongoose');
const flash = require('connect-flash');
const passport = require('passport');
const session = require('express-session');
const cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passwordless = require('passwordless');//passwordless
var MongoStore = require('passwordless-mongostore');
var email   = require('emailjs');
//Base de datos
mongoose.connect('mongodb://localhost:27017/login',{useNewUrlParser: true})
    .then( db => console.log('DB conected'))
    .catch(err => console.log(err));
//passport
require('./passport/local-auth');
require('./passport/google-auth');
//settings
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views')); 
app.set('view engine','pug');
app.use(express.static(__dirname +'/public'));
//passwordless
var smtpServer  = email.server.connect({
   user:    'cuantica.labs@gmail.com', 
   password: 'narutoverde123', 
   host:    'smtp.gmail.com', 
   ssl:     true
});

var pathToMongoDb = 'mongodb://localhost:27017/passwordless';
var host = 'http://localhost:3000/home/';

passwordless.init(new MongoStore(pathToMongoDb));
passwordless.addDelivery(
    function(tokenToSend, uidToSend, recipient, callback) {
        // enviar token
        smtpServer.send({
           text:    'Hola!\nPuedes acceder a tu cuenta ahora desde aqui: ' 
                + host + '?token=' + tokenToSend + '&uid=' + encodeURIComponent(uidToSend), 
           from:    'cuantica.labs@gmail.com', 
           to:      recipient,
           subject: 'Token for ' + host
        }, function(err, message) { 
            if(err) {
                console.log(err);
            }
            callback(err);
        });
});
//middlewares
app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
//app.use(express.urlencoded({extended: false}));
app.use(session({
    secret: 'sesionsecreta',
    resave: false,
    saveUninitialized: true 
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use((req,res,next)=>{
    app.locals.signupMessage=req.flash('signupMessage');
    app.locals.signinMessage=req.flash('signinMessage');
    app.locals.user=req.user;
    next();
});

app.use(passwordless.sessionSupport());
app.use(passwordless.acceptToken({ successRedirect: '/' }));

//configurando rutas
var indexRoutes = require('./routes/index');
//importando rutas
app.use('/',indexRoutes);
//Iniciando el servidor
app.listen(app.get('port'),()=>{
    console.log('Server on port', app.get('port'));
});