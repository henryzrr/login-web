
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/user');

passport.serializeUser((user,done)=>{
    done(null,user.id);
});
passport.deserializeUser(async (id,done)=>{
    const user = await User.findById(id);
    done(null,user);
});
passport.use('local-signup', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
},async (req,email,password,done)=>{
    const user = await User.findOne({'local.email': email});
    if(user){
        console.log(user);
        return done(null,false, req.flash('signupMessage','EL email ya ha sido registrado'));
    }else{
        const newUser = new User();
        newUser.local.email = email;
        newUser.local.password = newUser.encryptPassword(password);
        newUser.local.name= req.body.name;
        await newUser.save();
        
        return done(null,newUser);
    }
}));
passport.use('local-signin', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
  }, async (req, email, password, done) => {
    const user = await User.findOne({'local.email': email});
    if(!user) {
      return done(null, false, req.flash('signinMessage', 'EL usuario no existe'));
    }
    if(!user.validatePassword(password)) {
      return done(null, false, req.flash('signinMessage', 'Contraseña incorrecta, intente nuevamente'));
    }
    return done(null, user);
   console.log(user);
  }));

