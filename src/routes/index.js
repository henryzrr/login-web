const express = require('express');
const router = express.Router();
var passwordless = require('passwordless');
const indexController = require('../controllers/indexController');

router.get('/',indexController.index);
router.get('/signup',indexController.signupget);
router.post('/signup',indexController.signup);
router.get('/signin',indexController.signinget);
router.post('/signin',indexController.signinpost);
router.get('/logout',indexController.logout)
router.get('/home',isAuth, indexController.home);

router.get('/auth/google', indexController.authgoogle);

router.get('/auth/google/callback', indexController.googlecallback);

router.get('/auth/passwordless',indexController.loginPasswordless);

router.post('/auth/passwordless', 
	passwordless.requestToken(
		function(user, delivery, callback) {
			callback(null, user);
		}),
	function(req, res) {
  		res.render('correo');
});

function isAuth(req,res,next){
    if(req.isAuthenticated()){
        return next();
    }
    res.redirect('/');
}
module.exports = router;