  //google
  const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
  const passport = require('passport');
  const User = require('../models/user');
  
    passport.serializeUser((user, done) => {
        done(null, user);
    });
    passport.deserializeUser((user, done) => {
        done(null, user);
    });
passport.use(new GoogleStrategy({
    clientID: "1045971201202-338656nn6rbtvpg7tl6ili4o7kijjgke.apps.googleusercontent.com",
    clientSecret: "5o1yyftmhQJAGvMhf8WPVYNm",
    callbackURL: "http://localhost:3000/auth/google/callback"
	  },async (accessToken, refreshToken, profile, done) => {
	    console.log(profile);
        var user = await User.findOne({'google.id': profile.id});
	    if(user)
	    	return done(null, user);
	    else {
	    	var newUser = new User();
	    	newUser.google.id = profile.id;
	    	newUser.google.token = accessToken;
	    	newUser.google.name = profile.displayName;
	    	newUser.google.email = profile.emails[0].value;
            await newUser.save();
            return done(null, newUser);
	    }

    }));

